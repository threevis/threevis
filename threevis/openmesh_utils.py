
import openmesh as om
from .immediate import display_faces


def display_openmesh(mesh, normals=None, colors=None, uvs=None, shading='flat',
                     z_offset=0.5, texture=None, line_width=1, width=600,
                     height=400, background_color='#dddddd', clipping_planes=[],
                     show_bounds=False):
    display_faces(mesh.points(), mesh.face_vertex_indices(), normals=normals,
                  colors=colors, uvs=uvs, shading=shading, z_offset=z_offset,
                  texture=texture, line_width=line_width, width=width,
                  height=height, background_color=background_color,
                  clipping_planes=clipping_planes, show_bounds=show_bounds)


def display_file(path, normals=None, colors=None, uvs=None, shading='flat',
                 z_offset=0.5, texture=None, line_width=1, width=600,
                 height=400, background_color='#dddddd', clipping_planes=[],
                 show_bounds=False):
    m = om.TriMesh()
    om.read_mesh(m, path)

    display_openmesh(m, normals=normals, colors=colors, uvs=uvs,
                     shading=shading, z_offset=z_offset, texture=texture,
                     line_width=line_width, width=width, height=height,
                     background_color=background_color,
                     clipping_planes=clipping_planes, show_bounds=show_bounds)
