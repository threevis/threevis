from .indexed_attribute import FaceAttribute, PointAttribute, resolve_attributes, stretch_vertices
import numpy as np


def calculate_normal_edges(vertices, face_indices, normals, length):
    resolved_normals = None

    if isinstance(normals, FaceAttribute):
        resolved_normals = normals.values
        face_centers = []
        for face in face_indices:
            face_centers.append(np.average(vertices[face], axis=0, weights=face!=-1))
        vertices = face_centers
    elif face_indices is not None:
        resolved_normals = resolve_attributes(face_indices, [normals])[0]
        vertices, face_indices = stretch_vertices(vertices, face_indices)
    else:
        resolved_normals = normals

    edge_vertices = []
    edge_indices = []
    for i, vert in enumerate(vertices):

        norm = np.array(resolved_normals[i])
        vert = np.array(vert)

        edge_vertices.append(vert)
        edge_vertices.append(np.add(norm*length, vert))

        edge_indices.append([i*2, i*2 + 1])

    return edge_vertices, edge_indices


def calculate_face_normals(vertices, face_indices):
    v = np.asarray(vertices)
    f = np.asarray(face_indices)
    v0 = v[f][:, 0, :]
    v1 = v[f][:, 1, :]
    v2 = v[f][:, 2, :]
    fn = np.cross(v1 - v0, v2 - v0)
    fn /= np.linalg.norm(fn, axis=1)[:, np.newaxis]
    return FaceAttribute(fn)


def calculate_point_normals(vertices, face_indices):
    v = np.asarray(vertices)
    f = np.asarray(face_indices)
    fn = calculate_face_normals(v, f).values

    vi = np.ravel(f)
    fi = np.repeat(np.arange(f.shape[0]), f.shape[1], axis=0)
    vn = np.zeros_like(v)
    np.add.at(vn, vi[vi >= 0], (fn[fi])[vi >= 0])

    vn /= np.linalg.norm(vn, axis=1)[:, np.newaxis]
    return PointAttribute(vn)
