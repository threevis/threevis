.. meshvis documentation master file, created by
   sphinx-quickstart on Thu Feb 22 19:39:24 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to meshvis's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Setup
===================================

- Install dependencies
- Clone the repository
- pip install -e .

Dependencies
===================================

- [pythreejs](https://github.com/jovyan/pythreejs/) 1.0.0-beta4
- [openmesh-python](https://graphics.rwth-aachen.de:9000/adielen/openmesh-python) latest

Example
===================================


Overview
===================================

.. autosummary::
   :toctree: _autosummary

   meshvis.Context
   meshvis.immediate
   meshvis.mesh
   meshvis.openmesh_utils

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

